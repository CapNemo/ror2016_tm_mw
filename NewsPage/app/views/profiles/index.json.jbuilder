json.array!(@profiles) do |profile|
  json.extract! profile, :id, :display_name
  json.url profile_url(profile, format: :json)
end
