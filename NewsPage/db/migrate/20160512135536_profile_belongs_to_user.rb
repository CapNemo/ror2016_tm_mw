class ProfileBelongsToUser < ActiveRecord::Migration
  def change
    change_table :profiles do |t|
      t.belongs_to :user_id , :index => true, :foreign_key => true
    end
  end
end
